// grab our gulp packages

var gulp = require('gulp-help')(require('gulp')),
    gulpIf = require('gulp-if'),
    sass = require('gulp-sass'), // parse all sass files and transform them in css
    jshint = require('gulp-jshint'), // linter for my js files
    stylelint = require('gulp-stylelint'), // linter for my css files
    uglify = require('gulp-uglify'), // js minifyer
    del = require('del'), // allow deleting files or folders
    runSequence = require('run-sequence'), //allow chaining several functions, setting the correct order
    expect = require('gulp-expect-file'), // check is a file really exist
    browserSync = require('browser-sync').create();

// Transpile sass to css
gulp.task('sass', 'Transpile sass to css', function () {
    return gulp.src('src/public/scss/**/*.scss')
        .pipe(expect('src/public/scss/**/*.scss'))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('src/public/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Correcting js files
gulp.task('jshint', 'Correcting js files', function () {
    return gulp.src('src/public/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Checking sass files
gulp.task('stylelint', 'Checking sass files', function () {

    return gulp.src('src/public/scss/**/*.scss')
        .pipe(stylelint({
            configFile: 'config/.stylelintrc.json',
            failAfterError: false,
            reporters: [
                {
                    formatter: 'string',
                    console: true
                }
            ]
        }))
        .pipe(sass());
});

// Configure which files to watch and what tasks to use on file changes
gulp.task('watch', 'Configure which files to watch and what tasks to use on file changes', ['sass', 'browserSync'], function () {
    gulp.watch('src/public/js/**/*.js', ['jshint']);
    gulp.watch('src/public/scss/**/*.scss', ['stylelint', 'sass'], browserSync.reload);
    gulp.watch('src/public/**/*.html', browserSync.reload);
});

// Live reload, where the magic lives
gulp.task('browserSync', 'Live reload, where the magic lives', function () {
    browserSync.init({
        //proxy: "http://192.168.99.100"
        server: {
            baseDir: 'src/public/'
        }
    })
});



// BUILD SECTION HERE

gulp.task('generate-service-worker', function(callback) {
    var swPrecache = require('sw-precache');

    swPrecache.write('src/public/service-worker.js', {
        staticFileGlobs: [ 'src/public/**/*.{js,css,svg,eot,ttf,woff}'],
        stripPrefix: 'src/public'
    }, callback);
});

// Concatenate all the js files, following the right order
gulp.task('useref', 'concatenate all the js and css files, following the right order', function () {
    return gulp.src('src/public/*.html')
        .pipe(expect('src/public/*.html'))
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))     // Minifies only if it's a JavaScript file
        .pipe(gulpIf('*.css', cssnano()))   // Minifies only if it's a CSS file
        .pipe(gulp.dest('src/public/dist'));
});

// Minifies images  then moves them to dist folder
gulp.task('images', 'Minifies images  then moves them to dist folder', function () {
    return gulp.src('src/public/img/**/*.+(png|jpg|gif)')
        .pipe(expect('src/public/img/**/*.+(png|jpg|gif)'))
        .pipe(cache(imagemin()))
        .pipe(gulp.dest('src/public/dist/img'));
});

// Minifies html pages
gulp.task('minifyHtml', 'Minifies html pages', function () {
    return gulp.src('src/public/snippets/*.html')
        .pipe(expect('src/public/snippets/*.html'))
        .pipe(gulp.dest('src/public/dist/snippets/'))
        .pipe(gulp.src('src/public/dist/**/*.html'))
        .pipe(expect('src/public/dist/**/*.html'))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true,
            removeEmptyAttributes: true
        }))
        .pipe(gulp.dest('src/public/dist/'));
});

//Check if there are some fonts , then move them to dist folder
gulp.task('fonts', 'Check if there are some fonts , then move them to dist folder', function () {
    return gulp.src('src/public/css/webfonts/**/*.+(eot|svg|ttf|woff|woff2)')
        .pipe(expect('src/public/css/webfonts/**/*.+(eot|svg|ttf|woff|woff2)'))
        .pipe(gulp.dest('src/public/dist/css/webfonts/'));
});


// Deletes dist folder, in order to have everytime files updated
gulp.task('clean:dist', 'Deletes dist folder, in order to have everytime files updated', function () {
    return del.sync('dist');
});

// Deletes dist folder, in order to have everytime files updated
gulp.task('cache:clear', 'Deletes dist folder, in order to have everytime files updated', function (callback) {
    return cache.clearAll(callback);
});

gulp.task('criticalCss', function () {
    gulp.src('src/public/css/na-style.css')
        .pipe(criticalCss())
        .pipe(gulp.dest('dist'));
});

/**********************************************************
 * Main tasks
 **********************************************************/

gulp.task('default', 'Runs the Gulp task for development', function (callback) {
    runSequence(['sass','browserSync', 'watch'],
        callback
    );
});

gulp.task('build', 'Creates the files, corrected and minified, for production ', function (callback) {
    runSequence(['clean:dist', 'cache:clear'],
        ['images', 'sass', 'jshint', 'fonts', 'useref'],
        'minifyHtml',
        callback
    );
});
